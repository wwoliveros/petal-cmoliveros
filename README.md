## Overview

Thanks for taking the time to review my solution.  Here's an overview of the stack that I attempted to build the service with:

- Python/FastAPI for the basic REST API
- AWS EKS (and supporting EC2 worker nodes)
- Docker/Docker Hub for image build/storage
- Helm to deploy my containers to my EKS cluster
- Terraform for all infrastructure deployment

You can find supporting documentation in README.md files in each folder

**Final Thoughts**

In retrospect, I would have probably taken a simpler approach (something like ECS + Fargate), but I wanted a challenge, and to attempt to work with something that more closely represented the stack used by Petal.  

I encountered a compatibility issue troubleshooting container compatibility (long story short, I was building in ARM, and trying to run on x86) that took up some time trying to get my cluster up and running, which I ultimately got around by building locally with the docker buildx command, but ultimately this would have mattered if I integrated my repo with a build pipeline.

Unfortunately, I wasn't able to get ingress correctly configured in time, and while I was tempted to take more time to get things up and running, I instead opted to observe the time constraints imposed in the exercise, and deliver the repo as-is.

If you run the container locally, you will be able to prove out the microservice. 

Here was the Docker run command I used:

    docker run -p 8080:80 --rm cmoliveros/petal-devex:latest

Here is the cURL command I used.  Port 5000 was used by UPnP on my laptop, and while this seemed like a specific portion of the challenge, I was unable to create a working solution on a local machine that runs macOS.  Instead, I used port 8080 to demonstrate the concept of port publishing

    curl --location --request POST 'http://localhost:8080/v1/' \
    --header 'Content-Type: application/json' \
    --data-raw '{"content": "hello, world!"}'
