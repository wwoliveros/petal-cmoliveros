import requests
import json

from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()
print("API initialized")

# TODO: build logging module


class ReverseString(BaseModel):
    content: str


def shoutcloud(s: str):
    response = requests.post('HTTP://API.SHOUTCLOUD.IO/V1/SHOUT',
                             data=json.dumps({'INPUT': s}),
                             headers={'Content-Type': 'application/json'}
                             )
    return response.json()['OUTPUT']


@app.post("/v1")
async def create_architecture(s: ReverseString):
    return {'data': shoutcloud(s.content[::-1])}


if __name__ == '__main__':
    print(shoutcloud("this is a test string"[::-1]))
