
Run this command to build on x64, locally or in CI/CD:
  
    docker build --push -t cmoliveros/petal-devex:latest -f build/docker/Dockerfile .

Run this command to build locally on Apple Silicon:  

    docker buildx build --platform linux/amd64 --push -t cmoliveros/petal-devex:latest -f build/docker/Dockerfile .
