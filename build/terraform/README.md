## Deployment

**Prerequisites:**

AWS CLI Installed
https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html

AWS IAM Authenticator Installed
https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html

AWS API authentication, appropriate permissions

NOTE: I would have liked to have fleshed out a specific permission set/policy JSON, but in the interest of time I used administrator rights
https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html

K8s kubectl Installed
https://kubernetes.io/docs/tasks/tools/

WGET Installed
https://www.gnu.org/software/wget/

---

**Step 1**

Configure your AWS CLI with an existing API ID/key:

    $ aws configure
	    AWS Access Key ID [None]: YOUR_AWS_ACCESS_KEY_ID
	    AWS Secret Access Key [None]: YOUR_AWS_SECRET_ACCESS_KEY
	    Default region name [None]: YOUR_AWS_REGION 
	    Default output format [None]: json

**Step 2**

Navigate to the ./build/terraform/aws directory

**Step 3**

Run the following command in your terminal:

    terraform init

Then run:

    terraform apply
You will need to review the changes planned by Terraform, then approve them before deployment.  Deployment could take as long as 10 minutes to complete

**Step 4**

Run the following command to automatically configure kubctl to be able to communicate with your new EKS cluster

    aws eks \
    --region $(terraform output -raw region) \
    update-kubeconfig --name $(terraform output -raw cluster_name)


**Step 5**

Navigate to the **./build/terraform/aws** directory and repeat Step 3 to deploy the Helm chart to your cluster

## Note:
This is where I ended up running out of time with configuring my API to be reachable via a public IP.
