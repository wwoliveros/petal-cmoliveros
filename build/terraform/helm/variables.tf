variable "region" {
  default = "us-west-2"
}

variable "application_name" {
  type    = string
  default = "petal-devex"
}
